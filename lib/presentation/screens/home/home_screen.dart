/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutterdixe/core/widget_handler.dart';
import 'package:flutterdixe/presentation/screens/home/home_mobx.dart';
import 'package:flutterdixe/presentation/screens/home/home_mobx_auto.dart';
import 'package:provider/provider.dart';

import 'home_viewmodel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with CommonUIHandlers {
  HomeViewModel _homeViewModel = HomeViewModel();
  HomeMobx _homeMobx = HomeMobx();
  HomeMobxAuto _homeMobxAuto = MobxAutoneImpl();

  @override
  void initState() {
    scaffoldKey = GlobalKey<ScaffoldState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ChangeNotifierProvider.value(
          value: _homeViewModel,
          child: Consumer<HomeViewModel>(builder: (context, vm, _) {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              if (context != null) {
                handleCommonWidgetUI(context, vm);
              }
            });
            return _buildBody(context, vm);
          }),
        ),
      ),
    );
  }

  Container _buildBody(BuildContext context, HomeViewModel vm) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              child: InkWell(
                  onTap: () {
                    String route = "/demo?message=wtf&color_hex=ad9ee7";

//              Application.router.navigateTo(context, "homenavi");
                    vm.getUserInfo();
//                  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
//                    return DemoHomeNavigateScreen();
//                  }));
                  },
                  child: Text(vm.name)),
            ),
            Container(
              child: InkWell(
                onTap: () {
                  _homeMobx.plus();
                },
                child: Observer(
                  builder: (_) {
                    return Text(_homeMobx.counterObservable.value.toString());
                  },
                ),
              ),
            ),
            Container(
              child: InkWell(
                onTap: () {
                  _homeMobxAuto.plus();
                },
                child: Observer(
                  builder: (_) {
                    if(_homeMobxAuto.user==null){
                      return Text("wtf");
                    }
                    return Text(_homeMobxAuto?.user?.name);
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
