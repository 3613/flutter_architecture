import 'package:flutterdixe/core/usecases/BaseObservableUseCase.dart';
import 'package:flutterdixe/domain/usecase/get_user_info_usecase.dart';
import 'package:flutterdixe/presentation/model/user.dart';
import 'package:mobx/mobx.dart';
import 'package:rx/core.dart';

part 'home_mobx_auto.g.dart';

class MobxAutoneImpl = HomeMobxAuto with _$MobxAutoneImpl;

abstract class HomeMobxAuto with Store {
  GetUserInfoUseCase _getUserInfoUseCase = GetUserInfoUseCase();

  @observable
  User user;

  @action
  void plus() {
    Future.delayed(Duration(seconds: 1), () {
      _getUserInfoUseCase.call(
        NoParams(),
        observer: Observer(
          next: (next) {
            print("observableusecase done ${next}");
            user = next;
          },
          error: (err, [st]) {
            user = null;
          },
        ),
      );
    });
  }
}
