/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutterdixe/core/base_viewmodel/BaseViewModel.dart';
import 'package:flutterdixe/core/usecases/BaseObservableUseCase.dart';
import 'package:flutterdixe/domain/usecase/get_user_info_usecase.dart';
import 'package:rx/core.dart';

class HomeViewModel extends BaseViewModel {
  GetUserInfoUseCase _getUserInfoUseCase = GetUserInfoUseCase();
  String name = "kkk";

  void getUserInfo() {
    showDialog();
    Future.delayed(Duration(seconds: 3), () {
      _getUserInfoUseCase.call(
        NoParams(),
        observer: Observer(
          next: (next) {
            name = next.name;
            setSuccessMessage("vcl");
            dismissDialog();
            notifyListeners();
          },
          error: (err, [st]) {
            setErrorMessage(err);
          },
        ),
      );
    });
  }
}
