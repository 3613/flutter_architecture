// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_mobx_auto.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MobxAutoneImpl on HomeMobxAuto, Store {
  final _$userAtom = Atom(name: 'HomeMobxAuto.user');

  @override
  User get user {
    _$userAtom.context.enforceReadPolicy(_$userAtom);
    _$userAtom.reportObserved();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.context.conditionallyRunInAction(() {
      super.user = value;
      _$userAtom.reportChanged();
    }, _$userAtom, name: '${_$userAtom.name}_set');
  }

  final _$HomeMobxAutoActionController = ActionController(name: 'HomeMobxAuto');

  @override
  void plus() {
    final _$actionInfo = _$HomeMobxAutoActionController.startAction();
    try {
      return super.plus();
    } finally {
      _$HomeMobxAutoActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'user: ${user.toString()}';
    return '{$string}';
  }
}
