import 'package:mobx/mobx.dart';

class HomeMobx {
  Action plus;
  var counterObservable = Observable(0);

  HomeMobx() {
    plus = Action(_plus);
    reaction((_) => counterObservable.value, (data) {
      print("data ${data}");
    });
  }

  void _plus() {
    counterObservable.value++;
  }
}
