/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/foundation.dart';

class User {
  User({@required this.id, @required this.name});

  final String id;
  final String name;

  @override
  String toString() {
    return 'User{id: $id, name: $name}';
  }
}
