/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProgressDialog extends StatefulWidget {
  final Curve curve;

  ProgressDialog({
    this.curve,
  });

  @override
  State<StatefulWidget> createState() => _ProgressDialogState();
}

class _ProgressDialogState extends State<ProgressDialog> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation tween;

  @override
  void initState() {
    super.initState();
    controller = new AnimationController(vsync: this);
    tween = new Tween(begin: 0.0, end: 1.0).animate(controller);
    controller.animateTo(1.0,
        duration: new Duration(milliseconds: 300), curve: widget.curve ?? Curves.easeOutSine);
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(ProgressDialog oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listOfChildren = [];

    listOfChildren.add(new RefreshProgressIndicator());

    return new Center(
        child: new AnimatedBuilder(
            animation: controller,
            builder: (c, w) {
              return new ScaleTransition(
                scale: tween,
                child: new ClipRRect(
                  borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                  child: new Container(
                      color: Colors.transparent,
                      width: 100,
                      child: new Padding(
                        padding: new EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                        child: new Column(
                          mainAxisSize: MainAxisSize.min,
                          children: listOfChildren,
                        ),
                      )),
                ),
              );
            }));
  }
}
