/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/material.dart';
import 'package:flutterdixe/presentation/widgets/dialogs/progress_dialog.dart';

class MyDialog {
  static bool _isShowing = false;

  static void showLoading(
    BuildContext context, {
    Curve curve,
    String title,
    String subtitle,
    bool showCancelButton: false,
    Color cancelButtonColor,
    Color confirmButtonColor,
    String cancelButtonText,
    String confirmButtonText,
  }) {
    if (!_isShowing) {
      _isShowing = true;
      showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return new Container(
            color: Colors.transparent,
            child: new Padding(
              padding: new EdgeInsets.all(40.0),
              child: new Scaffold(
                backgroundColor: Colors.transparent,
                body: new ProgressDialog(
                  curve: curve,
                ),
              ),
            ),
          );
        },
      );
    }
  }

  static void dismiss(BuildContext context) {
    if (_isShowing) {
      _isShowing = false;
      Navigator.pop(context);
//      Navigator.pop(context);
    }
  }
}
