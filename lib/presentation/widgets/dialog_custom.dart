/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogCustom extends StatelessWidget {
  /// Center dialog title
  final String title;
  final String description;

  ///Button right
  final String buttonRightText;
  final Color buttonRightColor;
  final Function onPressedRightButton;

  ///Button left
  final String buttonLeftText;
  final Color buttonLeftColor;
  final Function onPressedLeftButton;
  final bool isDirectionButton;

  ///Icons in the top dialog
  final IconData icon;
  final Color circleColor;

  ///Flag show dialog
  static bool isShowDialog = false;

  DialogCustom(
      {@required this.title,
      @required this.description,
      @required this.buttonRightText,
      this.buttonRightColor,
      @required this.onPressedRightButton,
      this.buttonLeftText,
      this.buttonLeftColor,
      this.onPressedLeftButton,
      this.icon,
      this.circleColor,
      this.isDirectionButton = false});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        _childBodyDialog(context),
        _iconTopDialog(),
      ],
    );
  }

  _childBodyDialog(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 50,
        bottom: 8.0,
        left: 16.0,
        right: 16.0,
      ),
      margin: EdgeInsets.only(top: 45.0),
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 16.0),
          Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _getButtonLeft(context),
              buttonLeftText != null
                  ? SizedBox(
                      width: 8.0,
                    )
                  : Container(),
              Expanded(
                child: Container(
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      isShowDialog = false;
                      onPressedRightButton();
                    },
                    child: Text(
                      buttonRightText,
                      style: TextStyle(color: Colors.white),
                    ),
                    color: buttonRightColor,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _getButtonLeft(BuildContext context) {
    if (buttonLeftText == null) {
      return Container();
    }
    if (isDirectionButton) {
      return _getDirectionButtonLeft(context);
    }
    return _getConfirmButtonLeft(context);
  }

  Widget _getDirectionButtonLeft(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(0),
        child: FlatButton(
          shape: RoundedRectangleBorder(side: BorderSide(width: 2, color: Colors.red)),
          onPressed: () {
            Navigator.of(context).pop();
            isShowDialog = false;
            return onPressedLeftButton();
          },
          child: Text(
            buttonLeftText,
            style: TextStyle(color: Colors.red),
          ),
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _getConfirmButtonLeft(BuildContext context) {
    return Expanded(
      child: Container(
        child: FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
            isShowDialog = false;
            return onPressedLeftButton();
          },
          child: Text(
            buttonLeftText,
            style: TextStyle(color: Colors.white),
          ),
          color: buttonLeftColor,
        ),
      ),
    );
  }

  _iconTopDialog() {
    return Positioned(
      left: 16.0,
      right: 16.0,
      child: CircleAvatar(
        backgroundColor: circleColor ?? Colors.blueAccent,
        radius: 45.0,
        child: Icon(
          icon ?? Icons.info_outline,
          size: 50,
          color: Colors.white,
        ),
      ),
    );
  }

  static showConfirmDialog(
      BuildContext context, String title, String description, Function onConfirm,
      {String buttonRightText, String buttonLeftText}) async {
    if (isShowDialog) {
      return;
    }
    isShowDialog = true;

    await showDialog(
      context: context,
      builder: (BuildContext context) => DialogCustom(
        title: title,
        description: description,
        buttonRightText: buttonRightText ?? "Xác nhận",
        icon: Icons.info,
        circleColor: Colors.red,
        onPressedRightButton: () {
          onConfirm();
        },
        buttonRightColor: Colors.green,
        buttonLeftText: buttonLeftText ?? "Huỷ",
        buttonLeftColor: Colors.orange,
        onPressedLeftButton: () {},
      ),
    );
  }

  static showErrorDialog(BuildContext context, String title, String description) async {
    if (isShowDialog) {
      return;
    }
    isShowDialog = true;

    await showDialog(
      context: context,
      builder: (BuildContext context) => DialogCustom(
        title: title,
        description: description,
        buttonRightText: "OK",
        icon: Icons.close,
        circleColor: Colors.red,
        onPressedRightButton: () {},
        buttonRightColor: Colors.green,
      ),
    );
  }

  static showDirectionDialog(
    BuildContext context,
    String title,
    String description,
    String leftLabel,
    String rightLabel,
    Function onLeftConfirm,
    Function onRightConfirm,
  ) async {
    if (isShowDialog) {
      return;
    }
    isShowDialog = true;

    await showDialog(
      ///dismiss click around dialog to close
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => DialogCustom(
        title: title,
        description: description,
        buttonRightText: rightLabel,
        icon: Icons.check,
        circleColor: Colors.green,
        onPressedRightButton: () {
          onRightConfirm();
        },
        buttonRightColor: Colors.blue,
        buttonLeftText: leftLabel,
        buttonLeftColor: Colors.blue,
        onPressedLeftButton: () {
          onLeftConfirm();
        },
        isDirectionButton: true,
      ),
    );
  }

  static showMessageDialog(BuildContext context, String title, String description) async {
    if (isShowDialog) {
      return;
    }
    isShowDialog = true;

    await showDialog(
      context: context,
      builder: (BuildContext context) => DialogCustom(
        title: title,
        description: description,
        buttonRightText: "OK",
        icon: Icons.info,
        circleColor: Colors.blueAccent,
        onPressedRightButton: () {},
        buttonRightColor: Colors.blue,
      ),
    );
  }

  static showSuccessfulDialog(BuildContext context, String title, String description,
      {Function onDirection}) async {
    if (isShowDialog) {
      return;
    }
    isShowDialog = true;

    await showDialog<DialogCustom>(
      barrierDismissible: onDirection == null ? true : false,
      context: context,
      builder: (BuildContext context) => DialogCustom(
        title: title,
        description: description,
        buttonRightText: "OK",
        icon: Icons.check,
        circleColor: Colors.green,
        onPressedRightButton: () {
          if (onDirection != null) {
            onDirection();
          }
        },
        buttonRightColor: Colors.red,
      ),
    );
  }
}
