/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutterdixe/presentation/model/user.dart';
import 'package:rx/core.dart';

abstract class UserRepository {
  Observable<User> getUserInfo();
}
