/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutterdixe/core/usecases/BaseObservableUseCase.dart';
import 'package:flutterdixe/data/repo/user_repository_imp.dart';
import 'package:flutterdixe/presentation/model/user.dart';
import 'package:rx/core.dart';

class GetUserInfoUseCase extends BaseObservableUseCase<NoParams, User> {
  final userRepository = new UserRepositoryImp();

  Observable<User> buildUseCaseObservable(params) {
    return userRepository.getUserInfo();
  }
}
