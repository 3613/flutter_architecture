/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:rx/core.dart';
import 'package:rx/disposables.dart';

abstract class BaseObservableUseCase<PARAMS, OUT_PUT> {
  CompositeDisposable _compositeDisposable;

  // abstract method should be implement for each usecase
  Observable<OUT_PUT> buildUseCaseObservable(PARAMS params);

  // dart short invoking syntax
  void call(PARAMS params, {@required Observer<OUT_PUT> observer}) {
    _run(params, observer: observer);
  }

  void autoRun(PARAMS params) {
    run(params).subscribe(Observer(next: (nextValue) {
      print("autoRun call onNext ${nextValue}");
    }, error: (error, [st]) {
      print("autoRun call error ${error.toString()}");
    }));
  }

  Observable<OUT_PUT> run(PARAMS params) {
    return buildUseCaseObservable(params);
  }

  void _run(PARAMS params, {Observer<OUT_PUT> observer}) {
    var observable = run(params);
    if (observer != null) {
      addDisposable(observable.subscribe(observer));
    } else {
      observable.subscribe(Observer(next: (nextValue) {
        print("empty/null Observer call onNext ${nextValue}");
      }, error: (error, [st]) {
        print("empty/null Observer call error ${error.toString()}");
      }));
    }
  }

  void addDisposable(Disposable disposable) {
    if (_compositeDisposable == null) {
      _compositeDisposable = CompositeDisposable();
    }
    _compositeDisposable.add(disposable);
  }

  void disPose() {
    if (!_compositeDisposable.isDisposed) {
      _compositeDisposable.dispose();
      _compositeDisposable = null;
    }
  }
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}
