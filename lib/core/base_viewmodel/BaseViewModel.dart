/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/foundation.dart';
import 'package:flutterdixe/core/base_viewmodel/view_state.dart';
import 'package:flutterdixe/core/exception/BusinessExceptionMapper.dart';

class BaseViewModel extends ChangeNotifier {
  String errorMessage;
  String successMessage;
  ViewState viewState = ViewState.Idle;

  void setSuccessMessage(String message) {
    successMessage = message;
    viewState = ViewState.SuccessfulWithMessage;
    notifyListeners();
  }

  void _setErrorMessage(String message) {
    viewState = ViewState.Error;
    errorMessage = message;
    notifyListeners();
  }

  void showDialog() {
    viewState = ViewState.Loading;
    notifyListeners();
  }

  void dismissDialog() {
    viewState = ViewState.HideLoading;
    notifyListeners();
  }

  void setErrorMessage(Object exception) {
    BusinessExceptionMapper exMapper = BusinessExceptionMapper();
    // TODO use singleton pattern instead
    final String businessMessage = exMapper.mapToMessage(exception);
    _setErrorMessage(businessMessage);
    print("auto print mesage: $businessMessage");
  }
}
