/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

enum ViewState { Idle, Loading, HideLoading, Error, Successful, SuccessfulWithMessage }
