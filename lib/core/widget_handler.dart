/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/material.dart';
import 'package:flutterdixe/core/base_viewmodel/BaseViewModel.dart';
import 'package:flutterdixe/core/base_viewmodel/view_state.dart';
import 'package:flutterdixe/presentation/widgets/dialog_custom.dart';
import 'package:flutterdixe/presentation/widgets/dialogs/MyDialog.dart';

mixin CommonUIHandlers<T extends StatefulWidget> on State<T> {
  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  void initState() {
    print("init ");
    super.initState();
  }

  void handleCommonWidgetUI(BuildContext context, BaseViewModel vm) {
    switch (vm.viewState) {
      case ViewState.Loading:
        {
          MyDialog.showLoading(context);
        }
        break;
      case ViewState.HideLoading:
        {
          print("dissmiss dialog");
          MyDialog.dismiss(context);
        }
        break;

      case ViewState.Error:
        {
          showError(vm.errorMessage, context);
        }
        break;

      case ViewState.SuccessfulWithMessage:
        {
          showSuccessMessage(vm.successMessage, context);
        }
        break;
      default:
        break;
    }
  }

  void showSuccessMessage(String msg, BuildContext context) {
    DialogCustom.showSuccessfulDialog(context, "Thành công", msg);
  }

  void showError(String msg, BuildContext context) {
    final snackBar = SnackBar(
        content: Text(
      msg,
    ));

    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(snackBar);
    DialogCustom.showErrorDialog(context, "Sự cố", msg);
  }

  void hideMsg() {
    scaffoldKey.currentState..hideCurrentSnackBar();
  }

//  void hideLoading() {
//    if (_progressBar != null && _progressBar.isShowing()) {
//      _progressBar.hide(context);
//    }
//  }
//
//  void showLoading() {
//    if (_progressBar == null) {
//      _progressBar = new ProgressDialog(context, ProgressDialogType.Normal);
//    }
//    _progressBar.show(context);
//  }
//
//  void showErrorWithContext(String msg, BuildContext context) {
//    Scaffold.of(context)
//      ..hideCurrentSnackBar()
//      ..showSnackBar(SnackBar(content: Text('$msg'), backgroundColor: Colors.red));
//  }
//
//  void showLoadingWithContext(BuildContext context) {
//    if (_progressBar == null) {
//      _progressBar = new ProgressDialog(context, ProgressDialogType.Normal);
//    }
//    _progressBar.show(context);
//  }
//
//  void hideLoadingWithContext(BuildContext context) {
//    if (_progressBar != null && _progressBar.isShowing()) {
//      _progressBar.hide(context);
//    }
//  }
//
//  void showSuccessMessage(String msg, BuildContext context) {
//    Scaffold.of(context)
//      ..hideCurrentSnackBar()
//      ..showSnackBar(SnackBar(content: Text('$msg'), backgroundColor: Colors.green));
//    if (_progressBar != null && _progressBar.isShowing()) {
//      _progressBar.hide(context);
//    }
//  }
//
//  void showSuccessToast(BuildContext context, String msg, {int duration, int gravity, double backgroundRadius}) {
//    Toast.show(msg, scaffoldKey.currentContext,
//        duration: duration, gravity: gravity, backgroundColor: Color(0xFF81C784), textColor: Colors.white);
//  }
//
//  void showErrorToast(BuildContext context, String msg, {int duration, int gravity, double backgroundRadius}) {
//    Toast.show(msg, scaffoldKey.currentContext,
//        duration: duration, gravity: gravity, backgroundColor: Colors.red, textColor: Colors.white);
//  }
//
//  void showSimpleToast(BuildContext context, String msg, {int duration, int gravity, double backgroundRadius}) {
//    Toast.show(msg, scaffoldKey.currentContext, duration: duration, gravity: gravity);
//  }
//
//  void onWidgetDidBuild(Function callback) {
//    //wait and call this function after view is rendered
//    WidgetsBinding.instance.addPostFrameCallback((_) {
//      callback();
//    });
//  }
}
