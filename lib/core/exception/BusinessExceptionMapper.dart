/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutterdixe/core/exception/BussinessException.dart';

class BusinessExceptionMapper {
  String mapToMessage(Object exception) {
    var _errorMessage = "unknow";
    BusinessException ex;
    if (exception is DioError) {
      ex = _handleDio(exception);
    }
    if (exception is NoSuchMethodError) {
      ex = BusinessException("CustomBusinessException: NoSuchMethodError");
    }
    if (exception is BusinessException) {
      ex = exception;
    }
    if (exception is TypeError) {
      ex = BusinessException("CustomBusinessException: TypeError");
    }
    if (exception is SocketException) {
      ex = BusinessException("CustomBusinessException socket exception ${exception.message}");
    }
    if (exception is Exception) {
      ex = BusinessException("CustomBusinessException Exeption luôn ${exception.toString()}");
    }
    // final message returning
    if (ex == null) {
      return _errorMessage;
    }
    return ex.cause;
  }

  BusinessException _handleDio(DioError error) {
    String errorDescription = "";
    switch (error.type) {
      case DioErrorType.CANCEL:
        errorDescription = "Mã lỗi 499: Yêu cầu bị huỷ bỏ";
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        errorDescription = "Mã lỗi 408: Không có phản hồi từ hệ thống";
        break;
      case DioErrorType.DEFAULT:
        errorDescription = "Không có kết nối với internet. Hãy kiểm tra mạng và thử lại";
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        errorDescription = "Thời gian nhận phản hồi quá lâu. Xin thử lại";
        break;
      case DioErrorType.RESPONSE:
        errorDescription = "Received invalid status code: ${error.response.statusCode}";
        break;
      case DioErrorType.SEND_TIMEOUT:
        errorDescription = "Thời gian yêu cầu xử lý quá lâu. Xin thử lại";
        break;
    }

    if (error?.response?.statusCode != null) {
      if (error.response.statusCode == 429) {
        errorDescription = "Mã lỗi 429: Quá nhiều yêu cầu. Xin chờ một lát rồi hãy thử lại ";
      }
      if (error.response.statusCode == 500) {
        errorDescription = "Mã lỗi 500: Sự cố máy chủ";
      }
      if (error.response.statusCode == 404) {
        errorDescription = "Không tìm thấy tác vụ này trên hệ thống";
      }
    }

    if (error?.response?.data != null) {
      if (error?.response?.data?.runtimeType != String) {
        if (error?.response?.data['message'] != null) {
          errorDescription = error.response.data['message'].toString();
        }
        if (error?.response?.data['errors'] != null) {
          var errorMessage = error?.response?.data['errors'];
          if (errorMessage["error"] != null) {
            errorDescription = errorMessage["error"];
          }
        }
      }
    }
    return BusinessException(errorDescription);
  }
}
