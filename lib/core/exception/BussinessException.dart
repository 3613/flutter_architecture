/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

class BusinessException implements Exception {
  String cause;

  BusinessException(this.cause);

  @override
  String toString() {
    return "BusinessException's cause: -> $cause";
  }
}
// TODO Implement later
