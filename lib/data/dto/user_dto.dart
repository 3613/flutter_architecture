/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/foundation.dart';
import 'package:flutterdixe/data/mappers/base_dto_mapper.dart';
import 'package:flutterdixe/presentation/model/user.dart';

class UserDTO {
  UserDTO({@required this.id, @required this.name});

  final String id;
  final String name;
}

class UserDTOMapper extends BaseDTOMapper<UserDTO, User> {
  @override
  UserDTO mapToDTO(User entity) {
    return UserDTO(
      id: entity.id,
      name: entity.name,
    );
  }

  @override
  User mapToModel(UserDTO dto) {
    return User(
      id: dto.id,
      name: dto.name,
    );
  }
}
