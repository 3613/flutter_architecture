/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutterdixe/data/dto/user_dto.dart';
import 'package:flutterdixe/domain/repo/user_repository.dart';
import 'package:flutterdixe/presentation/model/user.dart';
import 'package:rx/constructors.dart' as rx;
import 'package:rx/core.dart';
import 'package:rx/src/core/observable.dart';

class UserRepositoryImp implements UserRepository {
  @override
  Observable<User> getUserInfo() {
    return rx.create((emitter) {
      UserDTO dto = new UserDTO(id: "clone", name: "HOANG CLONED");
      User user = UserDTOMapper().mapToModel(dto);
//      emitter.error(BusinessException("bla bla la"));
      emitter.next(user);
      emitter.complete();
    });
  }
}
