/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

abstract class BaseDTOMapper<DTO, MODEL> {
  DTO mapToDTO(MODEL entity);

  MODEL mapToModel(DTO dto);

  List<MODEL> mapToModels(List<DTO> dtos) {
    var models = List<MODEL>();
    if (dtos != null) {
      dtos.forEach((it) {
        MODEL entity = mapToModel(it);
        models.add(entity);
      });
    }
    return models;
  }

  List<DTO> mapToDTOs(List<MODEL> models) {
    var dtos = List<DTO>();
    if (dtos != null) {
      models.forEach((it) {
        dtos.add(mapToDTO(it));
      });
    }
    return dtos;
  }
}
