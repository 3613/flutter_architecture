/*
 * Created by Huu Hoang on 9/5/2020.
 * HOPEE Vietnam CO., LTD
 * hoangduchuuvn@gmail.com
 */

import 'package:flutter/material.dart';
import 'package:flutterdixe/presentation/navigate/app_component.dart';

void main() {
  runApp(AppComponent());
}
